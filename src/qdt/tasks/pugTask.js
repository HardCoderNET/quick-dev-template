const path = require('path');
const pugCompiler = require("../compilers/pugCompiler");
const { grouped_platforms } = require("../Library");

const _iif_pug = async function(_k_pug, platform, task) {
    const _raw_src = [];
    const _path = 'sources/' + _k_pug + '/pug/';
    const basePath = path.join(_path, task.path);
    const basePathFull = path.resolve(__dirname, '../../', basePath);

    for (var i = task.src.length - 1; i >= 0; i--) {
        _raw_src.push(path.resolve(__dirname, '../../', _path + task.src[i]));
    }

    await pugCompiler(basePathFull, platform, _raw_src, task.dest, task);
};

const pugTask = async (done) => {
    for (var k_pug in grouped_platforms) {
        for (var i = grouped_platforms[k_pug].length - 1; i >= 0; i--) {
            const _pug_s = grouped_platforms[k_pug][i].tasks.pug;

            for (var j = _pug_s.length - 1; j >= 0; j--) {
                await _iif_pug(k_pug, grouped_platforms[k_pug][i], grouped_platforms[k_pug][i].tasks.pug[j]);
            }
        }
    }

    done();
};

module.exports = pugTask;